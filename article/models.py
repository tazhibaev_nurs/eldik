from django.db import models


class ArticleCategory(models.Model):
    name = models.CharField(verbose_name="name_of_categery", max_length=64, blank=True, null=True)

    def __str__(self):
        return self.name


class Article(models.Model):
    name = models.CharField(verbose_name="name_of_articles", max_length=128, blank=True, null=True)
    text = models.TextField(verbose_name="description_of_text", blank=True, null=True)
    price = models.PositiveIntegerField(verbose_name="price_of_articles", blank=True, null=True)
    phone = models.CharField(verbose_name="phone_number", max_length=16, blank=True, null=True)
    category = models.ForeignKey(ArticleCategory, on_delete=models.SET_NULL, verbose_name="link_for_category",
                                 blank=True, null=True)


class ArticleImage(models.Model):
    img = models.ImageField(verbose_name="images", upload_to="article_image/", blank=True, null=True)
    article = models.ForeignKey(Article, on_delete=models.CASCADE, verbose_name="link_for_article", blank=True,
                                null=True)
