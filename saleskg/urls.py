from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from saleskg import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    path('articles/', include('article.urls'))
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
